﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/*
 * Class to represent a action done by the nurse on a patient
 */
[System.Serializable]
public class NurseAction
{
    public int id;
    public string name;
    public int gain;
    public int wrongUseLoss; // health to loos if used on wront patient
    public string type; //general = works for every patient, specific=work only for patient treament
    public int time;
}

[System.Serializable]
public class ActionsRoot
{
    public NurseAction[] actions;
}

public static class NurseActionParser
{
    public static ActionsRoot res;

    public static void parseActions()
    {

        string JSONPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "actions.json");

        string JSONComplete = File.ReadAllText(JSONPath);

        res = JsonUtility.FromJson<ActionsRoot>(JSONComplete);

    }
}
