using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedicalFile : MonoBehaviour
{
    public GameObject medicalFile;
    public Text medicalFileText;

    public void ConsultFile(bool open, int patientIndex)
    {
        medicalFile.SetActive(true);
        //show correct info
        if (patientIndex != -1)
        {
            //get patient file
            medicalFileText.text = PatientParser.patients.patients[patientIndex].getMedical();
        }
        else
        {
            medicalFileText.text = "(No patient...)";
        }
        if (open) 
        {
            StartCoroutine(ShowFile());
        }
        else
        {

            StartCoroutine(HideFile());
        }
            
    }

    public IEnumerator ShowFile()
    {
        Vector3 fileScale = medicalFile.transform.localScale;
        while (fileScale.x < 100)
        {
            fileScale = medicalFile.transform.localScale;
            fileScale.x += 10f;
            fileScale.y += 10f;
            medicalFile.transform.localScale = fileScale;
            yield return new WaitForSeconds(0.01f); // update interval
        }

    }

    public IEnumerator HideFile()
    {
        Vector3 fileScale = medicalFile.transform.localScale;
        while (fileScale.x > 0)
        {
            fileScale = medicalFile.transform.localScale;
            fileScale.x -= 10f;
            fileScale.y -= 10f;
            medicalFile.transform.localScale = fileScale;
            yield return new WaitForSeconds(0.01f); // update interval
        }
        medicalFile.SetActive(false);
    }

}
