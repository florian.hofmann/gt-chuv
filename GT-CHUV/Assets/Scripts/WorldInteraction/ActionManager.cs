using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionManager : MonoBehaviour
{
    public Text actionsText;
    [HideInInspector]
    public int currentPatientIndex;

    public AudioSource wrongTreatment;
    public AudioSource goodTreatment;

    private KeyCode[] actionKeys = new KeyCode[] {KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8 , KeyCode.Alpha9, KeyCode.Alpha0, KeyCode.P};

    // Start is called before the first frame update
    void Start()
    {
        //Modify GameObject to print it
        int key = 1;
        foreach(NurseAction nurseAction in NurseActionParser.res.actions)
        {
            //check for special case
            if (key==10) actionsText.text = actionsText.text + "[0] " + nurseAction.name;
            else if (key==11)actionsText.text = actionsText.text + "[P] " + nurseAction.name;
            else actionsText.text = actionsText.text + "[" + key + "] " + nurseAction.name;

            if (nurseAction.type == "general") actionsText.text = actionsText.text + " (No prescription required)";
            actionsText.text = actionsText.text + "\n";
            key++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Compute energy factor
        float energyFactor = 1f;
        if(DataHolder.nurseEnergy < 75)
        {
            energyFactor = 1.25f;
        }
        else if (DataHolder.nurseEnergy < 50)
        {
            energyFactor = 1.5f;
        }
        if (DataHolder.nurseEnergy < 25)
        {
            energyFactor = 1.75f;
        }
        if (DataHolder.nurseEnergy <=0)
        {
            energyFactor = 2.5f;
        }

        for (int i=0; i<actionKeys.Length; i++)
        {
            if (Input.GetKeyDown(actionKeys[i]))
            {
                //increment time
                DataHolder.time += (int)(NurseActionParser.res.actions[i].time * energyFactor);
                //check if treatment is specifi and if it's time
                if (NurseActionParser.res.actions[i].type == "general")
                {
                    goodTreatment.Play();
                    //increment healt of patient with a max of 100
                    if (PatientParser.patients.patients[currentPatientIndex].health + NurseActionParser.res.actions[i].gain >= 100) PatientParser.patients.patients[currentPatientIndex].health = 100;
                    else PatientParser.patients.patients[currentPatientIndex].health += NurseActionParser.res.actions[i].gain;
                }
                else
                {
                    //check if correct treatment
                    if(PatientParser.patients.patients[currentPatientIndex].treatmentAction == i + 1)
                    {
                        PatientParser.patients.patients[currentPatientIndex].doneTreatment += 1;
                        //check that the max treatment is not overflow
                        if (PatientParser.patients.patients[currentPatientIndex].doneTreatment <= PatientParser.patients.patients[currentPatientIndex].maxTreatment)
                        {
                            goodTreatment.Play();
                            PatientParser.patients.patients[currentPatientIndex].health = 100;  //restore all life
                        }
                        
                    }
                    else
                    {
                        wrongTreatment.Play();
                        PatientParser.patients.patients[currentPatientIndex].health -= NurseActionParser.res.actions[i].wrongUseLoss;   //apply treament loss in case of wrong use
                    }
                    
                }
            }
        }
        /*
        if (Input.GetKeyDown(KeyCode.Keypad1) || Input.GetKeyDown(KeyCode.Alpha1))
        {
            //increment time
            DataHolder.time += (int)(NurseActionParser.res.actions[0].time * energyFactor);
            //check if treatment is specifi and if it's time
            if(NurseActionParser.res.actions[0].type == "general")
            {
                //increment healt of patient with a max of 100
                if (PatientParser.patients.patients[currentPatientIndex].health + NurseActionParser.res.actions[0].gain >= 100) PatientParser.patients.patients[currentPatientIndex].health = 100;
                else PatientParser.patients.patients[currentPatientIndex].health += NurseActionParser.res.actions[0].gain;
            }
            else
            {
                //TODO if correct treatment, mark has taken and restore all life
                PatientParser.patients.patients[currentPatientIndex].health = 100;
            }
            
        }
        if (Input.GetKeyDown(KeyCode.Keypad2) || Input.GetKeyDown(KeyCode.Alpha2))
        {
            //increment time
            DataHolder.time += (int)(NurseActionParser.res.actions[1].time * energyFactor);
            //check if treatment is specifi and if it's time
            if (NurseActionParser.res.actions[0].type == "general")
            {
                //increment healt of patient with a max of 100
                if (PatientParser.patients.patients[currentPatientIndex].health + NurseActionParser.res.actions[1].gain >= 100) PatientParser.patients.patients[currentPatientIndex].health = 100;
                else PatientParser.patients.patients[currentPatientIndex].health += NurseActionParser.res.actions[1].gain;
            }
            else
            {
                //TODO if correct treatment, mark has taken and restore all life
                PatientParser.patients.patients[currentPatientIndex].health = 100;
            }
        }*/
        //TODO duplcate if with all keypad number

    }
}
