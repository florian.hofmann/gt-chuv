using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicalFileVarHolder : MonoBehaviour
{
    [HideInInspector]
    public int patientIndex = -1;
}
