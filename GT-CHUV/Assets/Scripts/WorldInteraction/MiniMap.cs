using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    public GameObject nurse;
    public GameObject stickman;
    public GameObject leftCoordMap;
    public GameObject rightCoordMap;
    public GameObject leftCoordWorld;
    public GameObject rightCoordWorld;

    private float leftxMap;
    private float rightxMap;
    private float leftxWorld;
    private float rightxWorld;

    private void Start()
    {
        //get left & right  x coords
        leftxMap = leftCoordMap.transform.position.x;
        rightxMap = rightCoordMap.transform.position.x;
        leftxWorld = leftCoordWorld.transform.position.x;
        rightxWorld = rightCoordWorld.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        //get left & right  x coords
        leftxMap = leftCoordMap.transform.position.x;
        rightxMap = rightCoordMap.transform.position.x;
        leftxWorld = leftCoordWorld.transform.position.x;
        rightxWorld = rightCoordWorld.transform.position.x;
        //map nurse coords to minimap coords
        float nurseX = nurse.transform.position.x;
        Vector3 vector = stickman.transform.position;
        vector.x = Remap(nurseX, leftxWorld, rightxWorld, leftxMap, rightxMap);
        stickman.transform.position = vector;


    }

    private float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
