using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Nurse : MonoBehaviour
{
    //Movement
    public float speed;
    private Rigidbody2D myRigidbody2D;
    private Vector3 vect;
    public Animator animator;

    //PopUps
    private MedicalFile medicalFileScript;
    private Action actionScript;

    //Interactions
    private bool isLocked = false;
    private bool canInteract = false;
    private bool isFolderOpen = false;
    private bool isActionOpen = false;
    private bool isOnPause = false;
    private string colliderTag = "";
    private Collider2D interactionCollider;
    private int patientIndex = -1;
    //energy management
    private int lastLossHour = Mathf.FloorToInt(DataHolder.beginTime / 60);

    //Input
    public GameManager gameManager;
    public GameObject medicalFile;  //medical file game object
    public GameObject action;   //action game object
    public SpriteRenderer nurseSprite;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody2D = GetComponent<Rigidbody2D>();
        medicalFileScript = (MedicalFile)medicalFile.GetComponent(typeof(MedicalFile));
        actionScript = (Action)action.GetComponent(typeof(Action));
    }

    // Update is called once per frame
    void Update()
    {
        //Check for interaction
        if (canInteract && Input.GetKeyDown(KeyCode.E))
        {
            switch (colliderTag)
            {
                case "Door":
                    ManageRoomInteraction();
                    break;

                case "Medical":
                    ManageMedicalInteraction();
                    break;

                case "Pause":
                    ManagePauseInteraction();
                    break;

                default:
                    break;
            }

        } 
        //Move
        if (isLocked) return;
        vect = Vector3.zero;
        vect.x = Input.GetAxisRaw("Horizontal");
        vect.y = Input.GetAxisRaw("Vertical");
        if (vect != Vector3.zero)
        {
            MoveCharacter();
        }
        AnimateCharacter();

        //check energy loss
        int currentHour = Mathf.FloorToInt(DataHolder.time / 60);
        if (currentHour > lastLossHour)
        {
            DataHolder.nurseEnergy -= DataHolder.lossEnergyByHour * (currentHour - lastLossHour);
            //check energy does not goes under 0
            if (DataHolder.nurseEnergy < 0) DataHolder.nurseEnergy = 0;
            lastLossHour = currentHour;
        }
    }

    //Manage interaction zones
    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (canInteract) return;
        canInteract = true;
        colliderTag = collider.tag;
        interactionCollider = collider;

        //show text zones
        /*
        switch (colliderTag)
        {
            case "Door":
                enterText.SetActive(true);
                consultText.SetActive(true);
                break;

            case "Pause":
                pauseText.SetActive(true);
                break;

            default:
                break;
        }*/
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        canInteract = false;

        //hide text zones
        /*
        switch (colliderTag)
        {
            case "Door":
                enterText.SetActive(false);
                consultText.SetActive(false);
                break;

            case "Pause":
                pauseText.SetActive(false);
                break;

            default:
                break;
        }*/
    }

    void MoveCharacter()
    {
        float energyFactor = (DataHolder.nurseEnergy > 50 ? (DataHolder.nurseEnergy / 100) : 0.5f);
        myRigidbody2D.MovePosition(transform.position + vect * speed * energyFactor);
    }

    void AnimateCharacter()
    {
        //modify anim
        animator.SetInteger("x", (int)vect.x);
        animator.SetInteger("y", (int)vect.y);
    }

    /*
     * Method to manage interaction with a patient room door
     */
    void ManageRoomInteraction()
    {
        //check if there is someone
        if (!isActionOpen) patientIndex = interactionCollider.GetComponent<Room>().Enter();
        if (patientIndex != -1)
        {
            isLocked = !isLocked;
            isActionOpen = !isActionOpen;
        }
        if (isActionOpen)
        {
            //open actions menu
            nurseSprite.enabled = false;
            actionScript.ShowAction(true, patientIndex);
        }
        else if (patientIndex != -1)
        {
            //close actions & consume time
            nurseSprite.enabled = true;
            actionScript.ShowAction(false, patientIndex);
            DataHolder.time += DataHolder.minutesToVisitRoom;
        }
    }

    /*
     * Method to manage interaction with the TV that show medical infos
     */
    void ManageMedicalInteraction()
    {
        //get patient index
        int patientIndex = interactionCollider.GetComponent<MedicalFileVarHolder>().patientIndex;
        isFolderOpen = !isFolderOpen;
        isLocked = !isLocked;
        if (isFolderOpen)
        {
            medicalFileScript.ConsultFile(true, patientIndex);
        }
        else
        {
            //close folder & consume time
            medicalFileScript.ConsultFile(false, -1);
            DataHolder.time += DataHolder.minutesToReadFolder;
        }
    }

    /*
        * Method to manage interaction with the pause room
        */
    void ManagePauseInteraction()
    {
        isOnPause = !isOnPause;
        //notify GameManager
        gameManager.SetIsNurseOnPause(isOnPause);
        //Manager UI
        if (isOnPause)
        {
            //hide nurse
            nurseSprite.enabled = false;
        }
        else
        {
            //show nurse
            nurseSprite.enabled = true;

        }
    }
}
