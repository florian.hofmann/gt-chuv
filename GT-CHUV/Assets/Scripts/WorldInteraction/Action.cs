using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : MonoBehaviour
{
    public GameObject action;

    public void ShowAction(bool open, int patientIndex)
    {
        //update currentPatientIndex of actionManager
        ActionManager am = GetComponent<ActionManager>();
        am.currentPatientIndex = patientIndex;
        //activate object
        action.SetActive(true);
        if (open)
        {
            StartCoroutine(OpenAction());
        }
        else
        {

            StartCoroutine(CloseAction());
        }

    }

    public IEnumerator OpenAction()
    {
        Vector3 actionScale = action.transform.localScale;
        while (actionScale.x < 100)
        {
            actionScale = action.transform.localScale;
            actionScale.x += 10f;
            actionScale.y += 10f;
            action.transform.localScale = actionScale;
            yield return new WaitForSeconds(0.01f); // update interval
        }

    }

    public IEnumerator CloseAction()
    {
        Vector3 actionScale = action.transform.localScale;
        while (actionScale.x > 0)
        {
            actionScale = action.transform.localScale;
            actionScale.x -= 10f;
            actionScale.y -= 10f;
            action.transform.localScale = actionScale;
            yield return new WaitForSeconds(0.01f); // update interval
        }
        action.SetActive(false);
    }
}
