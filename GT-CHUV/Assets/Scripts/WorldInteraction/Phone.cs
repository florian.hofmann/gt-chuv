using System.Collections;
using UnityEngine;

public class Phone : MonoBehaviour
{
    public SpriteRenderer phoneSprite;

    public void ShowPhone()
    {
        StartCoroutine(PopUpPhone());
    }

    private IEnumerator PopUpPhone()
    {
        phoneSprite.enabled = true;
        yield return new WaitForSeconds(3f);
        phoneSprite.enabled = false;

    }
}
