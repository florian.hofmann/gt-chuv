using UnityEngine;

public class DataHolder : MonoBehaviour
{
    public static string playerName = "";
    public static int playerScore = 0;

    public static int level = 1;

    public static int beginTime = 0; //in minutes
    public static int baseTime = 6; //hourse at day startup
    public static int endTime = 720; //minutes to end a day
    public static int time = beginTime;

    //Auto pass time
    public static float HowManySecondsForOneMinute = 5;
    public static float HowManySecondsForOneMinuteOnPause = 0.1f;

    //time consumtion
    public static int minutesToReadFolder = 15;
    public static int minutesToVisitRoom = 5;

    //energy consumtion
    public static int lossEnergyByHour = 10;
    public static int gainEnergyByMinOnPause = 1;
    public static float nurseEnergy = 100;
    public static int pauseTime = 0;

    //health consumtion
    public static int autoLossHealthFreq = 5;   //in min

    //Event
    public static int percentageOfTriggerEvent = 50; //% of chance thaht a event is triggered each ""timeSpaceBetweenEvents"" time
    public static int percentageOfVibratingPhone = 80; //% of chance that a patien will use the norification system when an event occurs (big health loss)
    public static int minLoss = 10;
    public static int maxLoss = 50;
    public static int timeSpaceBetweenEvents = 30; //in min

}
