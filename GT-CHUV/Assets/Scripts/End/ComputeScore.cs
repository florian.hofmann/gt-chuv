using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Net.Http;
using UnityEngine.Networking;

public class ComputeScore : MonoBehaviour
{
    public Text scoreText;

    //stats variable
    private int score = 0;
    private int deaths = 0;
    private int healthSum = 0;

    //sucess
    public Text successAngelText;
    public SpriteRenderer successAngelBadge;

    public Text successCareText;
    public SpriteRenderer successCareBadge;

    public Text successShapeText;
    public SpriteRenderer successShapeBadge;

    public Text successPauseText;
    public SpriteRenderer successPauseBadge;

    public Text sucessExecText;
    public SpriteRenderer successExecBadge;


    // Start is called before the first frame update
    void Start()
    {
        ComputeStats();
        ProcessScore();
        CheckSucess();

        //Update UI
        scoreText.text = scoreText.text + score + " pts";
        DataHolder.playerScore = score;
    }

    // Update is called once per frame
    void Update()
    {
        //check space pres
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // check if show leaderboard
            if (DataHolder.playerName != "")
            {
                SceneManager.LoadScene("Leaderboard");
            }
            else SceneManager.LoadScene("Menu");
        }
    }

    private void ComputeStats()
    {
        foreach(Patient p in PatientParser.patients.patients)
        {
            //count healthsum & deaths
            if (p.health > 0) healthSum += p.health;
            else deaths++;
        }
    }

    private void CheckSucess()
    {
        //angel success  : no deaths (give *2)
        if (deaths == 0)
        {
            Debug.Log("ang");
            //give bonus
            score *= 2;
            //update ui
            successAngelText.text = ": Guardian angel (x2 score)";
            successAngelBadge.enabled = true;
        }
        // success  :  all death (give +100)
        if (deaths == PatientParser.patients.patients.Length)
        {
            Debug.Log("Exec");
            //give bonus
            score += 100;
            //update ui
            successAngelText.text = ": Executioner (+99 pts)";
            successAngelBadge.enabled = true;
        }
        // success  :  mean health is over 50 (give *2)
        if ((healthSum/PatientParser.patients.patients.Length) > 0.5f)
        {
            Debug.Log("heal");
            //give bonus
            score = (int)(score * 2f);
            //update ui
            successCareText.text = ": Taking care of patients (x2 score)";
            successCareBadge.enabled = true;
        }
        // success  :  energy is over 75 % at the end of the game (+1000pts)
        if (DataHolder.nurseEnergy > 75)
        {
            Debug.Log("ener");
            //give bonus
            score += 10000;
            //update ui
            successShapeText.text = ": In shape (+1000 pts)";
            successShapeBadge.enabled = true;
        }
        // success  :  total pause time is 10% of the time total
        if (DataHolder.pauseTime >= (DataHolder.endTime/10))
        {
            Debug.Log("pause");
            //give bonus
            score += 500;
            //update ui
            successPauseText.text = ": Coffee addict (+500 pts)";
            successPauseBadge.enabled = true;
        }
    }

    private void ProcessScore()
    {
        score = (int)(healthSum * (PatientParser.patients.patients.Length - deaths) + DataHolder.nurseEnergy);
    }
}
