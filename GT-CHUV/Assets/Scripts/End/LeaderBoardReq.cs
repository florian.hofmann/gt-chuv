using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LeaderBoardReq : MonoBehaviour
{

    public Text leaderboardText;

    private static string url = "https://ramstein.internet-box.ch/gt_chuv/";

    // Start is called before the first frame update
    void Start()
    {
        //check if a player has been set
        if (DataHolder.playerName != "")
        {
            StartCoroutine(getRequest(url + "add?name=" + DataHolder.playerName + "&score=" + DataHolder.playerScore +"&level=" + DataHolder.level));
        }
    }

    // Update is called once per frame
    void Update()
    {   
        //check space pres
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("Menu");
        }
    }

    IEnumerator getRequest(string uri)
    {
        UnityWebRequest uwr = UnityWebRequest.Get(uri);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            leaderboardText.text = "Network error ...";
        }
        else
        {
            leaderboardText.text = uwr.downloadHandler.text;
            Debug.Log(uwr.downloadHandler.text);
            Debug.Log(uri);
        }
    }
}
