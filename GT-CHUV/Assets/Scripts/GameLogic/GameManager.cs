using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Time management
    public Text time;
    public Scrollbar timeBar;
    private bool isNurseOnPause = false;
    private float SecondsKeeper = 0;

    //Game Logic
    public List<Room> rooms;
    private int lastTimeDecreseHealth=0;
    private int lastEventTime = 0;

    //Audio
    public AudioSource music;
    public AudioSource phone;
    public AudioSource lowLife;

    public Phone phoneScript;

    // Start is called before the first frame update
    void Start()
    {
        //Parse Nurse Actin
        NurseActionParser.parseActions();
        //init list of patient
        PatientParser.parsePatients();
        //Shuffle room list for random attribution
        for (int i = 0; i < rooms.Count; i++)
        {
            Room temp = rooms[i];
            int randomIndex = Random.Range(i, rooms.Count);
            rooms[i] = rooms[randomIndex];
            rooms[randomIndex] = temp;

        }
        // Attributes rooms
        for (int i = 0; i < PatientParser.patients.patients.Length; i++)
        {
            rooms[i].patientIndex = i;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        bool playLowLife = false;
        foreach(Patient p in PatientParser.patients.patients)
        {
            //Auto Patient health loosing
            if (DataHolder.time - lastTimeDecreseHealth >= DataHolder.autoLossHealthFreq)
            {
                //Decrease health
                if(p.health>0) p.health -= p.autoLossHealth;
            }

            //Check if patient is low life
            if (p.health >= 0 && p.health < 33)
            {
                playLowLife = true;
            }
        }
        //update lastTimeDecreseHealth
        lastTimeDecreseHealth = DataHolder.time;
        //Check play audio low life
        if (playLowLife)
        {
            if (!lowLife.isPlaying)
            {
                lowLife.Play();
                music.volume = 0.2f;
            }
        }
        else
        {
            lowLife.Stop();
            music.volume = 0.6f;
        } 


        //Check if an even can try to be triggered
        if (DataHolder.time-lastEventTime >= DataHolder.timeSpaceBetweenEvents)
        {
            //randomize event trigger
            if (Random.Range(0, 100) < DataHolder.percentageOfTriggerEvent)
            {
                //Randomize patient
                int patientIndex = Random.Range(0, PatientParser.patients.patients.Length);
                //check that the patien is not dead
                if (PatientParser.patients.patients[patientIndex].health > 0)
                {
                    //randomize the fact that it wouild be a petiant joke or a true event
                    int fakeCheck = Random.Range(0, 100);
                    bool fake = false;
                    if (fakeCheck<=PatientParser.patients.patients[patientIndex].fakeAlertPercentage)
                    {
                        //fake alert
                        fake = true;
                        Debug.Log("Fake Event");

                    }
                    else
                    {
                        //true alert
                        Debug.Log("True Event");
                        //Randomize loss
                        int loss = Random.Range(DataHolder.minLoss, DataHolder.maxLoss + 1);
                        //Apply loss
                        PatientParser.patients.patients[patientIndex].health -= loss;
                    }

                    
                    
                    //randomize phone virbation trigger
                    if (fake || PatientParser.patients.patients[patientIndex].health > 0 && Random.Range(0, 100) < DataHolder.percentageOfVibratingPhone)
                    {
                        phoneScript.ShowPhone();
                        phone.Play();
                        //Show light on door
                        foreach (Room r in rooms)
                        {
                            if (patientIndex == r.patientIndex)
                            {
                                r.alert.enabled = true;
                                r.alertMap.enabled = true;
                                break;
                            }
                        }
                    }
                }
            }
            //update lastEventTime
            lastEventTime = DataHolder.time;
        }

        //check for auto time passing
        SecondsKeeper += Time.deltaTime;
        if (isNurseOnPause)
        {
            //Nurse is taking a break, auto time faster
            if (SecondsKeeper >= DataHolder.HowManySecondsForOneMinuteOnPause)
            {
                SecondsKeeper = 0;
                DataHolder.time++;
                DataHolder.pauseTime++;
                //gain energy
                if (DataHolder.nurseEnergy + DataHolder.gainEnergyByMinOnPause <= 100) DataHolder.nurseEnergy += DataHolder.gainEnergyByMinOnPause;
            }
        }
        else
        {
            //Nurse is working, auto time properly
            if (SecondsKeeper >= DataHolder.HowManySecondsForOneMinute)
            {
                SecondsKeeper = 0;
                DataHolder.time++;
            }
        }
        
        //check time
        if (DataHolder.time >= DataHolder.endTime) SceneManager.LoadScene("End");
        int hours = Mathf.FloorToInt(DataHolder.time / 60) + DataHolder.baseTime;
        float minutes = Mathf.FloorToInt(DataHolder.time % 60);
        time.text = string.Format("{0:00}:{1:00}", hours, minutes);
        //update timebar
        timeBar.size = ((float)DataHolder.time / (float)DataHolder.endTime);

    }

    /*
     * Method to use by the nurse to notify the fact shw is taking a break
     */
    public void SetIsNurseOnPause(bool isOnPause)
    {
        isNurseOnPause = isOnPause;
        //Modify Audio Speed
        if (isOnPause) music.pitch = 1.75f;
        else music.pitch = 1;

    }
}
