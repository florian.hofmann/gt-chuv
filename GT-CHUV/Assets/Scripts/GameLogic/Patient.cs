using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class Patient
{
    //Attribure
    //private int Hapiness { get; set; }//0-100;
    public string name;
    public int health; //0-100;
    public string illness;
    public int autoLossHealth;
    public int treatmentAction;
    public int maxTreatment;
    public int fakeAlertPercentage;
    public int doneTreatment = 0;



    public string getMedical()
    {
        string txt = name + "\nCondition :\n- " + illness + "\nTreatment :\n- " + NurseActionParser.res.actions[treatmentAction-1].name + " ( " + doneTreatment + " / "+maxTreatment+" )";
        return txt;
    }
}

[System.Serializable]
public class PatientsRoot
{
    public Patient[] patients;
}

public static class PatientParser
{
    public static PatientsRoot patients;

    public static void parsePatients()
    {

        //string JSONPath = Path.Combine(Application.dataPath, "Config.json");
        string JSONPath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "patients" + DataHolder.level + ".json");

        string JSONComplete = File.ReadAllText(JSONPath);

        patients = JsonUtility.FromJson<PatientsRoot>(JSONComplete);

    }
}
