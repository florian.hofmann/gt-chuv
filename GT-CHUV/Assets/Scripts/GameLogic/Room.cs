using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Room : MonoBehaviour
{
    [HideInInspector]
    public int patientIndex = -1;
    public Scrollbar lifeBar;
    public GameObject lifeBarGO;
    public GameObject heartIcon;
    public GameObject deathIcon;
    public GameObject unknownIcon;
    public GameObject medicalFileCollider;
    private MedicalFileVarHolder medicalFileVarHolder;

    private bool isDiscovered = false;
    private bool isDead = false;

    public AudioSource deathSound;

    public SpriteRenderer alert;
    public SpriteRenderer alertMap;

    // Start is called before the first frame update
    void Start()
    {
        medicalFileVarHolder = (MedicalFileVarHolder)medicalFileCollider.GetComponent(typeof(MedicalFileVarHolder));
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDiscovered)
        {
            medicalFileVarHolder.patientIndex = patientIndex;  //setup patientindex in medical collider
            return;
        }
        //check if room is not ocupied
        if (patientIndex == -1) return;
        //check patient health
        if (!isDead && PatientParser.patients.patients[patientIndex].health <= 0)
        {
            isDead = true;
            //play sound
            deathSound.Play();
            if (isDiscovered)
            {
                deathIcon.SetActive(true);
                heartIcon.SetActive(false);
                lifeBarGO.SetActive(false);
            }
        }
        if (!isDead && patientIndex != -1)
        {
            float health = PatientParser.patients.patients[patientIndex].health / 100f;
            lifeBar.size = health;
            //TODO chaange color depending on health level
            //if (health > 0.66f) lifeBar.colors.colorMultiplier = 
            if (health <= 0)
            {
                isDead = true;
                heartIcon.SetActive(false);
                deathIcon.SetActive(true);
            }
        }
    }

    public int Enter()
    {
        alert.enabled = false;
        alertMap.enabled = false;
        if (!isDiscovered) Discover();
        if (isDead) return -1;
        return patientIndex;
    }

    void Discover()
    {
        unknownIcon.SetActive(false);
        if (!isDead && patientIndex != -1)
        {
            heartIcon.SetActive(true);
            lifeBarGO.SetActive(true);

        }
        isDiscovered = true;
    }
}
