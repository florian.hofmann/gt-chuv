using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Text playerName;
    
    // Update is called once per frame
    void Update()
    {
        //check space pres
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            DataHolder.level = 1;
            startGame();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            DataHolder.level = 2;
            startGame();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            DataHolder.level = 3;
            startGame();
        }
    }

    void startGame()
    {
        //load username
        DataHolder.playerName = playerName.text;
        SceneManager.LoadScene("Hospital");
        //reset datas
        DataHolder.nurseEnergy = 100;
        DataHolder.time = DataHolder.beginTime;
        DataHolder.pauseTime = 0;
    }
}
